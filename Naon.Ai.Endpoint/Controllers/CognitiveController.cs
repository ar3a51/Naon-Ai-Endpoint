﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Naon.Ai.Endpoint.Models;
using Naon.Ai.Endpoint.Models.DocumentModels;
using Naon.Ai.Endpoint.Repositories;
using Naon.Ai.Endpoint.Services;

using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

namespace Naon.Ai.Endpoint.Controllers
{
    [Route("api")]
    [ApiController]
    public class CognitiveController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private readonly IPostRepository _postRepository;
        private readonly IQueryCognitiveService _queryCognitiveService;

        public CognitiveController(
            IOptionsMonitor<AppSettings> optionsMonitor,
            IPostRepository postRepository,
            IQueryCognitiveService queryCognitiveService)
        {
            _appSettings = optionsMonitor.CurrentValue;
            _postRepository = postRepository;
            _queryCognitiveService = queryCognitiveService;
        }

        [HttpGet("test")]
        public ActionResult<String> Test()
        {
            return "system is working";
        }
          
        // GET api/values/5
        [HttpGet("{postId}")]
        public async Task<ActionResult<PostDocument>> Get(string postId)
        {
            var resultPost = await _postRepository.GetPost(postId);
            ImageAnalysis imageAnalysis = await _queryCognitiveService.QueryService(resultPost);
            resultPost.Image.Description = imageAnalysis.Description.Captions[0].Text;
            resultPost.Image.Tags = imageAnalysis.Description.Tags.ToArray();
            await _postRepository.UpdatePostDocument(resultPost);
            return resultPost;
        }
    }
}
